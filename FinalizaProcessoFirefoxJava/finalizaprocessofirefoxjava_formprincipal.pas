unit FinalizaProcessoFirefoxJava_FormPrincipal;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, Buttons,
  StdCtrls, Process;

type

  { TFormPrincipal }

  TFormPrincipal = class(TForm)
    ButtonSim: TButton;
    ButtonNao: TButton;
    Label1: TLabel;
    Label2: TLabel;
    procedure ButtonNaoClick(Sender: TObject);
    procedure ButtonSimClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
    Sim: Boolean;
    procedure CmdInterrup;
  end;

var
  FormPrincipal: TFormPrincipal;

implementation

{$R *.lfm}

{ TFormPrincipal }

procedure TFormPrincipal.FormCreate(Sender: TObject);
var
  Indice: SmallInt;
begin
  Sim := false;

  Left := (Screen.Width  div 2) - (Width div 2);
  Top  := (Screen.Height div 2) - (Height div 2);

  if ParamCount > 0 then
    for Indice := 0 to ParamCount do
      if LowerCase(Trim(ParamStr(Indice))) = 'sim' then Sim := true;
end;

procedure TFormPrincipal.FormShow(Sender: TObject);
begin
  if Sim then CmdInterrup;
end;

procedure TFormPrincipal.CmdInterrup;
var
  ProcessCmdInterrup: TProcess;
begin
  ProcessCmdInterrup := TProcess.Create(FormPrincipal);

  ProcessCmdInterrup.Executable := 'TASKKILL.exe';

  ProcessCmdInterrup.Parameters.Add('/F');
  ProcessCmdInterrup.Parameters.Add('/IM');
  ProcessCmdInterrup.Parameters.Add('firefox.exe');
  ProcessCmdInterrup.Parameters.Add('/IM');
  ProcessCmdInterrup.Parameters.Add('java.exe');
  ProcessCmdInterrup.Parameters.Add('/IM');
  ProcessCmdInterrup.Parameters.Add('javaw.exe');
  ProcessCmdInterrup.Parameters.Add('/IM');
  ProcessCmdInterrup.Parameters.Add('javaws.exe');
  ProcessCmdInterrup.Parameters.Add('/IM');
  ProcessCmdInterrup.Parameters.Add('javacpl.exe');
  ProcessCmdInterrup.Parameters.Add('/IM');
  ProcessCmdInterrup.Parameters.Add('jp2launcher.exe');

  ProcessCmdInterrup.ShowWindow := swoHIDE;
  ProcessCmdInterrup.Options    := ProcessCmdInterrup.Options + [poWaitOnExit];

  ProcessCmdInterrup.Execute;

  ProcessCmdInterrup.Free;

  Close;
end;

procedure TFormPrincipal.ButtonSimClick(Sender: TObject);
begin
  CmdInterrup;
end;

procedure TFormPrincipal.ButtonNaoClick(Sender: TObject);
begin
  Close;
end;

end.

