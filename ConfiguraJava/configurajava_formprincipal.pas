unit configurajava_formprincipal;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, Process,
  LResources, StdCtrls;

type

  { TFormPrincipal }

  TFormPrincipal = class(TForm)
    ButtonNao: TButton;
    ButtonSim: TButton;
    Label1: TLabel;
    procedure ButtonNaoClick(Sender: TObject);
    procedure ButtonSimClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
    Instalar: Boolean;
    Desinstalar: Boolean;
    InstalarUsuario: Boolean;
    DesinstalarUsuario: Boolean;
    procedure ExecutaRegedit;
  end;

var
  FormPrincipal: TFormPrincipal;

implementation

{$R *.lfm}

{ TFormPrincipal }

procedure TFormPrincipal.FormCreate(Sender: TObject);
var
  Indice: SmallInt;
  ProcessCmdRegedit: TProcess;
begin
  Instalar    := false;
  Desinstalar := false;

  InstalarUsuario    := false;
  DesinstalarUsuario := false;

  Left := (Screen.Width  div 2) - (Width div 2);
  Top  := (Screen.Height div 2) - (Height div 2);

  if ParamCount > 0 then
    for Indice := 0 to ParamCount do
      if LowerCase(Trim(ParamStr(Indice))) = 'instalar_aplicativo' then begin
        Instalar := true;
      end else begin
        if LowerCase(Trim(ParamStr(Indice))) = 'desinstalar_aplicativo' then begin
          Desinstalar := true;
        end else begin
          if LowerCase(Trim(ParamStr(Indice))) = 'instalar_usuario' then begin
            InstalarUsuario := true;
          end else begin
            if LowerCase(Trim(ParamStr(Indice))) = 'desinstalar_usuario' then begin
              DesinstalarUsuario := true;
            end;
          end;
        end;
      end;
end;

function MemoryStreamToString(M: TMemoryStream) : string;
begin
  SetString(Result, PChar(M.Memory), M.Size div SizeOf(Char));
end;

procedure TFormPrincipal.ExecutaRegedit;
const
  bufsize = 1023;
var
  Res: TLResource;
  Stream: TStream;
  MemoryStreamReg: TMemoryStream;
  FileStreamReg: TStringStream;
  ProcessCmdRegedit: TProcess;
  StringListReg: TStringList;
  PastaInstalacao: String;
  ArquivoRegistro: String;
  ResRegistro: String;
  Contador: ShortInt;
begin
  ResRegistro := BoolToStr(Instalar, 'JavaPortavelHklm', '') +
                 BoolToStr(Desinstalar, 'JavaPortavelHklmDesinstalar', '') +
                 BoolToStr(InstalarUsuario, 'JavaPortavelHkcu', '') +
                 BoolToStr(DesinstalarUsuario, 'JavaPortavelHkcuDesinstalar', '');

  Res             := LazarusResources.Find(ResRegistro);
  Stream          := TLazarusResourceStream.CreateFromHandle(Res);
  StringListReg   := TStringList.create;
  MemoryStreamReg := TMemoryStream.Create;
  PastaInstalacao := StringReplace(ExtractFilePath(ParamStr(0)), '\', '*', [rfReplaceAll, rfIgnoreCase]);

  ArquivoRegistro := BoolToStr(Instalar, 'JavaPortavelHklm.temp.reg', '') +
                     BoolToStr(Desinstalar, 'JavaPortavelHklmDesinstalar.temp.reg', '') +
                     BoolToStr(InstalarUsuario, 'JavaPortavelHkcu.temp.reg', '') +
                     BoolToStr(DesinstalarUsuario, 'JavaPortavelHkcuDesinstalar.temp.reg', '');

  MemoryStreamReg.CopyFrom(Stream, Stream.Size);

  if InstalarUsuario then begin
    try
      StringListReg.LoadFromFile(GetEnvironmentVariableUTF8('userprofile') + '\AppData\LocalLow\Sun\Java\Deployment\deployment.properties');

      if Pos('deployment.security.level=MEDIUM', StringListReg.Text) = 0 then begin
        StringListReg.Add('deployment.security.level=MEDIUM');

        StringListReg.SaveToFile(GetEnvironmentVariableUTF8('userprofile') + '\AppData\LocalLow\Sun\Java\Deployment\deployment.properties');
      end;
    except
    end;
  end else begin
    StringListReg.Text := StringReplace(MemoryStreamToString(MemoryStreamReg), '<PastaInstalacaoJavaPortavel>', StringReplace(PastaInstalacao, '*', '\\', [rfReplaceAll, rfIgnoreCase]), [rfReplaceAll, rfIgnoreCase]);

    StringListReg.SaveToFile(ArquivoRegistro);

    ProcessCmdRegedit := TProcess.Create(FormPrincipal);

    ProcessCmdRegedit.Executable := 'regedit.exe';

    ProcessCmdRegedit.Parameters.Add('/S');
    ProcessCmdRegedit.Parameters.Add(ArquivoRegistro);

    ProcessCmdRegedit.ShowWindow := swoHIDE;
    ProcessCmdRegedit.Options    := ProcessCmdRegedit.Options + [poWaitOnExit];

    ProcessCmdRegedit.Execute;

    ProcessCmdRegedit.Free;
  end;

  MemoryStreamReg.Free;
  StringListReg.free;
end;

procedure TFormPrincipal.FormShow(Sender: TObject);
begin
  if Instalar or Desinstalar or InstalarUsuario or DesinstalarUsuario then begin
    ExecutaRegedit;

    Close;
  end;
end;

procedure TFormPrincipal.ButtonSimClick(Sender: TObject);
begin
  Instalar    := false;
  Desinstalar := false;

  InstalarUsuario    := true;
  DesinstalarUsuario := false;

  ExecutaRegedit;

  Close;
end;

procedure TFormPrincipal.ButtonNaoClick(Sender: TObject);
begin
  Instalar    := false;
  Desinstalar := false;

  InstalarUsuario    := false;
  DesinstalarUsuario := false;

  //ExecutaRegedit;

  Close;
end;

initialization
  {$I JavaPortavel.lrs}

end.

